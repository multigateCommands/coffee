#! /usr/bin/perl -w

use strict;
use LWP::Simple;

my $coffee = get("https://real-italian-coffee.herokuapp.com/get-word");
print "All out of coffee!\n" and exit unless defined $coffee;

# Only print first 250 chars from first line, just in case.
$coffee = substr (( split /\n/, $coffee )[0], 0, 250);

print "$coffee\n";
